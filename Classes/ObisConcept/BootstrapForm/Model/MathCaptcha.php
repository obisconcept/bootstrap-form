<?php

namespace ObisConcept\BootstrapForm\Model;

use Neos\Flow\Annotations as Flow;

/**
 * @Flow\Scope("session")
 */
class MathCaptcha {

    /**
     * Variable one
     *
     * @var int
     */
    protected $varOne = 0;

    /**
     * Variable two
     *
     * @var int
     */
    protected $varTwo = 0;

    /**
     * Result
     *
     * @var int
     */
    protected $result = 0;

    /**
     * Init math task
     *
     * @Flow\Session(autoStart = TRUE)
     * @return void
     */
    public function initMathTask() {

        $this->varOne = mt_rand(1,20);
        $this->varTwo = mt_rand(1,20);

        $this->result = $this->varOne + $this->varTwo;

    }

    /**
     * Get variable one
     *
     * @return int
     */
    public function getVarOne() {

        return $this->varOne;

    }

    /**
     * Get variable two
     *
     * @return int
     */
    public function getVarTwo() {

        return $this->varTwo;

    }

    /**
     * Get result
     *
     * @return int
     */
    public function getResult() {

        return $this->result;

    }

}