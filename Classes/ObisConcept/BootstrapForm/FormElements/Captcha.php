<?php

namespace ObisConcept\BootstrapForm\FormElements;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Log\SystemLoggerInterface;

class Captcha extends \Neos\Form\Core\Model\AbstractFormElement {

    /**
     * Session manager
     *
     * @Flow\Inject
     * @var \ObisConcept\BootstrapForm\Model\MathCaptcha
     */
    protected $mathCaptcha;

    /**
     * Actual result
     *
     * @var int
     */
    protected $result = 0;

    /**
     * Init form element
     *
     * @return void
     */
    public function initializeFormElement() {

        if ($this->mathCaptcha->getResult() == 0) {

            $this->mathCaptcha->initMathTask();

        } else {

            $this->result = $this->mathCaptcha->getResult();
            $this->mathCaptcha->initMathTask();

        }

        $this->setRenderingOption('varOne', $this->mathCaptcha->getVarOne());
        $this->setRenderingOption('varTwo', $this->mathCaptcha->getVarTwo());

    }

    /**
     * On form submit
     *
     * @param \Neos\Form\Core\Runtime\FormRuntime $formRuntime
     * @param mixed $elementValue
     * @return void
     */
    public function onSubmit(\Neos\Form\Core\Runtime\FormRuntime $formRuntime, &$elementValue) {

        if ($this->result != (int) $elementValue) {

            $processingRule = $this->getRootForm()->getProcessingRule($this->getIdentifier());
            $processingRule->getProcessingMessages()->addError(new \Neos\Error\Messages\Error('Captcha math task isn\'t correct', 8732423746));

        }

    }

}