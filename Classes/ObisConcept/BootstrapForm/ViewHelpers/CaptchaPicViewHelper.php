<?php

namespace ObisConcept\BootstrapForm\ViewHelpers;

use Neos\FluidAdaptor\Core\ViewHelper\AbstractTagBasedViewHelper;

/**
 * Class CaptchaPicViewHelper
 *
 * @package ObisConcept\BootstrapForm
 * @subpackage ViewHelpers
 */
class CaptchaPicViewHelper extends AbstractTagBasedViewHelper {

    /**
     * Image html tag
     *
     * @var string
     */
    protected $tagName = 'img';

    /**
     * Render image tag
     *
     * @param int $width
     * @param int $height
     * @return string
     */
    public function render($width = 100, $height = 50) {

        $string = $this->renderChildren();

        $this->tag->addAttribute('src', 'data:image/jpeg;base64,'.$this->generatePicture($string, $width, $height));
        $this->tag->addAttribute('alt', 'Captcha');
        $this->tag->addAttribute('border', '0');

        return $this->tag->render();

    }

    /**
     * Generate picture
     *
     * @param string $string
     * @param int $width
     * @param int $height
     * @return string
     */
    protected function generatePicture($string, $width, $height) {

        ob_start ();

        $image = ImageCreate($width, $height);
        $white = ImageColorAllocate($image, 255, 255, 255);
        $black = ImageColorAllocate($image, 0, 0, 0);
        $grey = ImageColorAllocate($image, 204, 204, 204);

        ImageFill($image, 0, 0, $black);

        ImageString($image, 3, 30, 3, $string, $white);

        ImageRectangle($image,0,0,$width-1,$height-1,$grey);
        imageline($image, 0, $height/2, $width, $height/2, $grey);
        imageline($image, $width/2, 0, $width/2, $height, $grey);
        ImageJpeg($image);

        $imageData = ob_get_contents();
        ImageDestroy($image);

        ob_end_clean();

        return base64_encode($imageData);

    }

}