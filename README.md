# Neos CMS - Twitter Bootstrap form plugin
Change form templates to Twitter Bootstrap layout for Neos CMS.

## Installation
Add the package in your site package composer.json
```
"require": {
    "obisconcept/bootstrap-form": "~1.1"
}
```

## Version History

### Changes in 1.1.0
- New 1.1 version for bootstrap package

### Changes in 1.0.7
- New form element captcha feature

### Changes in 1.0.5
- Bugfix

### Changes in 1.0.4
- Optimized radio buttons element

### Changes in 1.0.3
- Fixed single select dropdowns having wrong class

### Changes in 1.0.2
- Critical Bugfix

### Changes in 1.0.1
- Bugfixes
- New Label CSS Class

### Changes in 1.0.0
- First version of the Neos CMS bootstrap form plugin
- Updated Elements: Page, Section, Form, Field, SingleLineText, Checkbox, MultiLineText, Navigation